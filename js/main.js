// for (var i = 0; i < 5; i++) {     (function (x) {
// setTimeout(function(){             console.log(x),x *1000         })
// })(i); }

// var globalVar = "xyz";

// (function outerFunc(outerArg) {
//     var outerVar = 'a';

//     (function innerFunc(innerArg){
//         var innerVar = 'b';

//         console.log(
//             "outerArg = "+outerArg+"\n"+
//             "innerArg = "+innerArg+"\n"+
//             "outerArg = "+outerVar+"\n"+
//             "outerArg = "+innerVar+"\n"+
//             "globalVar = "+globalVar+"\n");
//     })(456);
// })(123);

// var add = (function () {
//     var counter = 0;
//     return function () {return counter += 1;}
// })();

// add();
// add();
// add();
// console.log(add());

// var car = (function car() {
//     var model = 'no model';
//      return {
//         setModel: function(str) { model = str; },
//         getModel: function() { return model; }
//      }
// })();
// car.setModel('sussita');
// console.log(car.getModel()); // no model

// var Person = {
//     firstName:'kfir',
//     lastName:'evron',

//     message:function(text,i){
//         console.log(this.firstName+' '+this.lastName+' '+i);
//     }
// }

// var mike = {
//     firstName:'mike',
//     lastName:'evron'
// }
// Person.message.call(mike,'dsdsf');

// function isFullAge(){

//     var arr = Array.prototype.slice.call(arguments);

//     console.log(arr);
// }

// isFullAge(1990,1985,1233);
// var myPromise = new Promise(function(resolve, reject) {
//     resolve(paramater); // Promise resolved! 🙂
//     reject(parameter2); // Promise rejected! 🙁
// });

// myPromise.then(function(paramater) {
// console.log('Got data! Promise fulfilled.');
// }, function(parameter2) {
// console.log('Promise rejected.');
// })
// var myPromise = new Promise(function (resolve, reject) {
//     var request = new XMLHttpRequest();

//     request.open('GET', 'https://api.icndb.com/jokes/random');
//     request.onload = function () {
//         if (request.status == 200) {
//             resolve(request.response); // we got data here, so resolve the Promise
//         } else {
//             reject(Error(request.statusText)); // status is not 200 OK, so reject
//         }
//     };

//     request.onerror = function () {
//         reject(Error('Error fetching data.')); // error occurred, reject the  Promise
//     };

//     request.send(); //send the request
// });

// myPromise.then(function (data) {
//     console.log('Got data! Promise fulfilled.');
//     document.getElementsByTagName('body')[0].textContent = JSON.parse(data).value.joke;
// }, function (error) {
//     console.log('Promise rejected.');
//     console.log(error.message);
// })

// var objProto = {
//     calculateAge: function () {
//         return (2017 - this.yearOfBirth);
//     }
// };

// var jhon = Object.create(objProto);

// jhon.job = 'teacher';
// jhon.yearOfBirth = 1990;
// jhon.name = 'jhon';

// jhon.calculateAge()

//passing functions as arguments
// var years = [2000, 1985, 1980];

// function arrayCalc(arr, fn) {
//   var arrRes = [];
//   for (var i = 0; i < arr.length; i++) {
//     arrRes.push(fn(arr[i]));
//   }
//   return arrRes;
// }

// function calculateAges(el) {
//   return 2017 - el;
// }
// debugger;
// function isFullAge(limit, el) {
//   return el >= limit;
// }
// var ages = arrayCalc(years, calculateAges);

// var fullJapan = arrayCalc(ages, isFullAge.bind(this, 20));

// console.log(ages);
// console.log(fullJapan);

//Functions returning functions

// function interviewQuestion(job) {
//   if (job === "designer") {
//     return function(name) {
//       console.log(name + ", can you please explain UX");
//     };
//   } else if (job === "teacher") {
//     return function(name) {
//       console.log("What subject do you teach, " + name);
//     };
//   } else {
//     return function(name) {
//       console.log("hello " + name + " what do you do ?");
//     };
//   }
// }

// var teacherQuestion = interviewQuestion("teacher");

// interviewQuestion("designer")("mark");

// Lecture : Bind,call,apply

// ES6

// var images = [
//     { height: "34px", width: "239px" },
//     { height: "334px", width: "139px" },
//     { height: "314px", width: "339px" }
// ];

// var heights;

// heights = images.map(image => {
//     return image.height;
// });

// console.log(heights);

// Rest
// function product(...numbers) {
//     return numbers.reduce(function(acc, number) {
//         return acc + number;
//     }, 0);
// }

// var rest = product(1, 2, 3, 4, 5);
// console.log(rest);

// Spread
// function join(array1, array2) {
//     return [...array1, ...array2];
// }

// var arr1 = ["a", "b", "c"];
// var arr2 = ["d", "e", "f"];
// var spread = join(arr1, arr2);
// console.log(spread);

// function unshift(arr) {
//     return [...arr, "f"];
// }

// var arr = ["a", "b", "c", "d", "e"];

// var test = unshift(arr);

// console.log(test);

// const classes = [
//     ["Chemistry", "9AM", "Mr. Darnick"],
//     ["Physics", "10:15AM", "Mrs. Lithun"],
//     ["Math", "11:30AM", "Mrs. Vitalis"]
// ];

// let classesAsObject;

// classesAsObject = classes.map(([subject, time, teacher]) => {
//     return { subject, time, teacher };
// });
// console.log(classesAsObject);

// const numbers = [1, 2, 3];

// function double(...numbers) {

//     return [number] = numbers;
// }
// debugger;
// let test = double(numbers);
// console.log(test);

// class Monster {
//     constructor(options) {
//         this.health = 100;
//         this.name = options.name;
//     }
// }

// class Snake extends Monster {
//     constructor(options) {
//         super(options);
//         console.log(options);
//     }

//     bite() {
//         return "bite";
//     }
// }

// let a = new Snake({ title: "hi", color: "red" });
// a.bite();
// const url = "https://jsonplaceholder.typicode.com/posts/";
// fetch(url).then(data => console.log(data));

// (function() {
//     console.log("this is the start");

//     setTimeout(function cb() {
//         console.log("this is a msg from call back");
//     });

//     console.log("this is just a message");

//     setTimeout(function cb1() {
//         console.log("this is a msg from call back1");
//     }, 0);

//     console.log("this is the end");
// })();
// var factor = function(number) {
//     var result = 1;
//     var count;
//     for (count = number; count > 1; count--) {
//       result *= count;
//     }
//     return result;
//   };
//   console.log(factor(6));

// function proveIt() {
//     console.log(this);
// }

// document.getElementById("clicker").addEventListener("click", promisify());
// var attitude = function(original, replacement, callback) {
//     return function(callback) {
//         return callback.replace(original, replacement);
//     };
// };

// var snakify = attitude(/millenials/gi, "Snake People");
// var hippify = attitude(/baby boomers/gi, "Aging Hippies");

// console.log(snakify("The Millenials are always up to something."));
// // The Snake People are always up to something.
// console.log(hippify("The Baby Boomers just look the other way."));
// The Aging Hippies just look the other way.

// var test = function(original, replacement, callback) {
//     return function(callback) {
//         return new Promise(function(resolve, reject) {
//             if (callback) {
//                 console.log(callback);
//                 resolve(original + replacement);
//             } else {
//                 reject("err");
//             }
//         });
//     };
// };

// var promisify = test(1, 0);
// promisify().then(function(a) {
//     console.log(a);
// });

// var countdown = function(value) {
//     if (value > 0) {
//         console.log(value);
//         return countdown(value - 1);
//     } else {
//         return value;
//     }
// };
// countdown(10);
// var factorial = function(number) {
//     if (number <= 0) {
//         // terminal case
//         return 1;
//     } else {
//         // block to execute
//         return number * factorial(number - 1);
//     }
// };
// console.log(factorial(6));

var Person = {
    name: "kfir",
    age: 33
};

Object.assign(Person, {
    job: "Developer"
});

console.log(Person);
